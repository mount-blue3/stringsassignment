function getString(testArray){
    if(testArray.length === 0){
        return "";
    }
    else{
        return testArray.join(" ");
    }
}
module.exports = getString;