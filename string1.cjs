function convertToNumbers(testString){
    let val = Number(testString.replace("$","").replace(",",""));
    if(!isNaN(val)){
        return val;
    }
    else{
        return 0;
    }
    
}
module.exports = convertToNumbers;