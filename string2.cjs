function splitting(testString){
    let result = testString.split(".");
    let resArray=[];
    for(let index=0;index<result.length;index++){
        let temp = Number(result[index]);
        if(isNaN(temp)){
            return [];
        }
        else{
            resArray.push(temp);
        }
    }
    return resArray;
}
module.exports = splitting;