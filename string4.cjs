function fullName(testObject){
    let result =[];
    for(let key in testObject){
        let low = testObject[key].toLowerCase();
        let temp = low.charAt(0);
        testObject[key] = temp.toUpperCase()+low.slice(1);
        result.push(testObject[key]);
    }
    return result.join(" ");
}
module.exports = fullName;