function getMonth(testString){
    let month = {1:"january",2:"feb",3:"Mar",4:"april",5:"may",6:"june",7:"july",8:"aug",9:"sep",10:"oct",11:"nov",12:"dec"};
    let m = testString.split("/")
    let result=m[1];
    for(let key in month){
        if(key === result){
            return month[key];
        }
    }
    return;
}
module.exports = getMonth;